public class Complex {
    //поставлено два переноса

    private final double FIRST_VARIABLE;  //я не поняла, что означают имена переменных, поэтому просто пронумеровала
    private final double SECOND_VARIABLE; //я не поняла, что означают имена переменных, поэтому просто пронумеровала


    public Complex(double FIRST_VARIABLE, double SECOND_VARIABLE) {
        if ((Double.isNaN(FIRST_VARIABLE)) || (Double.isNaN(SECOND_VARIABLE))) { //поставила скобки для читабельности
            throw new ArithmeticException();
        }
        this.FIRST_VARIABLE = FIRST_VARIABLE;
        this.SECOND_VARIABLE = SECOND_VARIABLE;
    }


    public double realPart()      { return FIRST_VARIABLE; }
    public double imaginaryPart() { return SECOND_VARIABLE; }


    public Complex add(Complex comp) {
        return new Complex(FIRST_VARIABLE + comp.FIRST_VARIABLE,
                SECOND_VARIABLE + comp.SECOND_VARIABLE);  //перенос на другую строку для читабельности
    }


    @Override public boolean equals(Object obj) {  //дала более понятное имя объекта для Object
        if (obj == this) {   //не стояли фигурные скобки
            return true;
        }
        if (!(obj instanceof Complex)) { //не стояли фигурные скобки
            return false;
        }
        Complex comp = (Complex) obj; //дала более понятное имя объекта для Complex
        return (Double.compare(FIRST_VARIABLE, comp.FIRST_VARIABLE) == 0) && //поставила скобки для читабельности
                (Double.compare(SECOND_VARIABLE, comp.SECOND_VARIABLE) == 0);
    }


    @Override public int hashCode() {
        int result = 17 + Double.hashCode(FIRST_VARIABLE);
        result = (31 * result) + Double.hashCode(SECOND_VARIABLE); //поставила скобки для читабельности
        return result;
    }


    @Override public String toString() {
        return "(" + FIRST_VARIABLE
                + (SECOND_VARIABLE < 0 ? "" : "+") //перенос на другую строку для читабельности
                + SECOND_VARIABLE + "i)";
    }


    public static void main(String[] args) {
        Complex complex = new Complex(1, 2); //дала более понятное имя объекта для Complex
        System.out.println(complex.add(complex));
    }
}


/*
package ua.solution.narozhnyy;

public class Complex {
	private final double re;
    private final double im;

    public Complex(double re, double im) {
        if (Double.isNaN(re) || Double.isNaN(im)) {
            throw new ArithmeticException();
        }
        this.re = re;
        this.im = im;
    }

    public double realPart()      { return re; }
    public double imaginaryPart() { return im; }

    public Complex add(Complex c) {
        return new Complex(re + c.re, im + c.im);
    }

    @Override public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Complex))
            return false;
        Complex c = (Complex) o;
        return Double.compare(re, c.re) == 0 &&
               Double.compare(im, c.im) == 0;
    }

    @Override public int hashCode() {
        int result = 17 + Double.hashCode(re);
        result = 31 * result + Double.hashCode(im);
        return result;
    }

    @Override public String toString() {
        return "(" + re + (im < 0 ? "" : "+") + im + "i)";
    }

    public static void main(String[] args) {
        Complex z = new Complex(1, 2);
        System.out.println(z.add(z));
    }
}
 */